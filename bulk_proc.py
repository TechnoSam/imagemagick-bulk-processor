import argparse
from pathlib import Path

from bulk_proc import is_convert_installed, convert_path


def main():
    parser = argparse.ArgumentParser(description='Process multiple images using ImageMagick')
    parser.add_argument('source', metavar='SOURCE', type=str, nargs='+',
                        help='Path(s) to source images or directories')
    parser.add_argument('-o', '--output-directory', help='Where to place the converted images')
    parser.add_argument('-m', '--max-res', type=int, default=1920, help='Maximum resolution to scale the images to')
    parser.add_argument('-f', '--output-format', default='jpg', help='File type to output')
    parser.add_argument('-k', '--keep-original', action='store_true', help='Keep the original image')

    args = parser.parse_args()

    if not is_convert_installed():
        print('ImageMagick is not installed or cannot be found on your PATH')
        exit(1)

    output_directory = Path(args.output_directory) if args.output_directory is not None else Path('.')
    output_format = args.output_format
    max_res = args.max_res
    keep_original = args.keep_original
    for path in args.source:
        path = Path(path)
        convert_path(path, output_directory, output_format, max_res, keep_original)


if __name__ == '__main__':
    main()
