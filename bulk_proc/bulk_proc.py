import subprocess
from pathlib import Path


def is_convert_installed():
    command = ['convert', '-h']
    try:
        test = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    except FileNotFoundError:
        return False
    stdout, stderr = test.communicate()
    return test.returncode == 0


def convert_image(path: Path, output_directory: Path, output_format: str, max_res: int, keep_original: bool):
    input_name = path.stem
    output_path = output_directory.joinpath(f'{input_name}.{output_format}')
    command = ['convert', str(path), '-resize', f'{max_res}x{max_res}', str(output_path)]

    convert = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = convert.communicate()
    if convert.returncode != 0:
        print(f'Failed to convert {path} (cmd: {" ".join(command)})')
        return

    if not keep_original:
        path.unlink()

    print(f'Completed: {str(path)}')


def convert_path(path: Path, output_directory: Path, output_format: str, max_res: int, keep_original: bool):
    if path.is_file():
        convert_image(path, output_directory, output_format, max_res, keep_original)
    elif path.is_dir():
        for p in path.glob('*'):
            convert_path(p, output_directory, output_format, max_res, keep_original)
    else:
        print(f'Cannot convert: {path}')
